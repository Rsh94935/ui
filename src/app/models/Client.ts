export interface Client {
    client_ID: number;
    first_name: string;
    last_name: string;
    street_no: string;
    street_name: string;
    town: string;
    county: string;
    post_code: string;
    subscribed: boolean;
    email_address: string;
    pw: string;
    uuid: string;
    logged_in: string;
}

export class ClientClass implements Client {
    client_ID: number;
    first_name: string;
    last_name: string;
    street_no: string;
    street_name: string;
    town: string;
    county: string;
    post_code: string;
    subscribed: boolean;
    email_address: string;
    pw: string;
    uuid: string;
    logged_in: string;

    constructor(item: Client) {
        for (let key in item) {
            this[key] = item[key];
        }
    }
}