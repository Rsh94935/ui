import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './clientPortal/home/home.component';
import { AboutUsComponent } from './clientPortal/about-us/about-us.component';
import { PropertiesComponent } from './clientPortal/properties/properties.component';
import { EventsComponent } from './clientPortal/events/events.component';
import { ServicesComponent } from './clientPortal/services/services.component';



const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'about-us', component: AboutUsComponent },
  { path: 'homes', component: PropertiesComponent },
  { path: 'events', component: EventsComponent },
  { path: 'services', component: ServicesComponent },
  { path: '',   redirectTo: '/home', pathMatch: 'full' }, // redirect to `home`
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
