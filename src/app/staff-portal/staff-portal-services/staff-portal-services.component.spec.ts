import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaffPortalServicesComponent } from './staff-portal-services.component';

describe('StaffPortalServicesComponent', () => {
  let component: StaffPortalServicesComponent;
  let fixture: ComponentFixture<StaffPortalServicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaffPortalServicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaffPortalServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
