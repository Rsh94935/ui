import { Injectable } from '@angular/core';
import { RequestOptions, Http, Headers } from '@angular/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StaffPortalService {

  constructor(private http: Http) { }

  public getStaffDetails(internalType, staff_ID): Observable<any> {
    const headers : Headers = new Headers();
    const opts : RequestOptions = new RequestOptions();
    headers.append('staff_ID', staff_ID);
    headers.append("Content-Type", "application/json")
    headers.append('Access-Control-Allow-Headers', 'Origin, Content-Type, X-Auth-Token, Access-Control-Allow-Headers, Access-Control-Allow-Origin, Access-Control-Allow-Methods');
    headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
    headers.append('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,DELETE,PUT');
    headers.append('internalType', internalType);
    headers.append('uuid', localStorage.getItem('staff_uuid'));
    headers.append('search_ID', staff_ID);
    headers.append('searchInternalType', internalType);
    opts.url = "http://localhost:8080/uk/GetStaffDetails";
    opts.method  ="GET";
    opts.headers = headers;

    return this.http.request("GET", opts);
  }

  public getGoogleAPIAuth(): Observable<any> {
    const headers : Headers = new Headers();
    headers.append("Content-Type", "application/json")
    headers.append('Access-Control-Allow-Headers', 'Origin, Content-Type, X-Auth-Token, Access-Control-Allow-Headers, Access-Control-Allow-Origin, Access-Control-Allow-Methods');
    headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
    headers.append('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,DELETE,PUT');
    const opts : RequestOptions = new RequestOptions();
    opts.url = "http://localhost:8080/uk/GetAPIKey";
    opts.method  ="GET";
    opts.headers = headers;

    return this.http.request("GET", opts);
  }
}
