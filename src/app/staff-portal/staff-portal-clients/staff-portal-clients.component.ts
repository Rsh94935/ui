import { Component, OnInit, Input } from '@angular/core';
import { StaffPortalClientsService } from './staff-portal-clients-service';

@Component({
  selector: 'app-staff-portal-clients',
  templateUrl: './staff-portal-clients.component.html',
  styleUrls: ['./staff-portal-clients.component.scss']
})
export class StaffPortalClientsComponent implements OnInit {
  @Input() staff;
  @Input() staffDetails;
  @Input() key;
  clientList = [];
  images;
  active = 0;
  deletedIDs;

  constructor(private spc: StaffPortalClientsService) { }

  ngOnInit() {
    this.setEventList();
  }

  private setEventList() {
    this.clientList = [];
    this.spc.getClientList(this.staff['staff_ID']).subscribe(data => {
      this.clientList = JSON.parse(data['_body']);
    });
    this.active = 0;
  }

  public createGrid() {
    let rows = "";

    this.clientList.forEach(res => {
      if ( rows === "" ) {
        rows = "40px";
      } else {
        rows += " 40px";
      }
    })

    const style = {
      'display': 'grid',
      'grid-template-columns': '1fr',
      'grid-template-rows' : rows
    }

    return style;
  }

  public checkRights(rights) {
    if ( this.staffDetails['admin'] ) {
      return true;
    }
    if ( this.staffDetails[rights] === true ) {
      return true;
    }

    return false;
  }
  
  public contactClient(i) {
    const title = prompt("Enter email subject:", "");
    const message = prompt("Enter email contents:", "");

    this.spc.contactClient(this.staff['staff_ID'], this.clientList[i]['client_ID'], title, message).subscribe(res => {
      if ( res._body === "Insufficient rights" ) {
        alert("User is not authorized to email");
      }
    }) 
  }

  public deleteClient(i) {
    this.spc.deleteClient(this.staff['staff_ID'], this.clientList[i]['client_ID']).subscribe(res => {
      if ( res._body === "Client deleted" ) {
        const newList = [];
        let minus = 0;
        for ( let loopCounter = 0; loopCounter < this.clientList.length; loopCounter++ ) {
          if ( loopCounter !== i ) {
            newList[loopCounter - minus] = this.clientList[loopCounter];
          } else {
            minus = 1;
          }
        }
        this.clientList = newList;
      }
    });
  }
}
