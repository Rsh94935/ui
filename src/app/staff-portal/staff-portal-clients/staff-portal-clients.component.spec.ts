import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaffPortalClientsComponent } from './staff-portal-clients.component';

describe('StaffPortalClientsComponent', () => {
  let component: StaffPortalClientsComponent;
  let fixture: ComponentFixture<StaffPortalClientsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaffPortalClientsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaffPortalClientsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
