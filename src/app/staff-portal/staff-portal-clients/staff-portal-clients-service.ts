import { Injectable } from '@angular/core';
import { RequestOptions, Http, Headers } from '@angular/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StaffPortalClientsService {

  constructor(private http: Http) { }

  public getClientList(staff_ID): Observable<any> {
    const headers : Headers = this.createHeaders(staff_ID);
    headers.append('start', "0");
    headers.append('amount', "25");
    const opts : RequestOptions = new RequestOptions();
    opts.url = "http://localhost:8080/uk/GetClientList";
    opts.method  ="GET";
    opts.headers = headers;

    return this.http.request("GET", opts);
  }

  private createHeaders(staff_ID) {
      const uuid = localStorage.getItem("staff_uuid");
      const headers : Headers = new Headers();
      headers.append('staff_ID', staff_ID);
      headers.append("Content-Type", "application/json")
      headers.append('Access-Control-Allow-Headers', 'Origin, Content-Type, X-Auth-Token, Access-Control-Allow-Headers, Access-Control-Allow-Origin, Access-Control-Allow-Methods');
      headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
      headers.append('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,DELETE,PUT');
      headers.append('uuid', uuid);

      return headers;
  }

  public contactClient(staff_ID, client_ID, subject, message): Observable<any> {
    const headers : Headers = this.createHeaders(staff_ID);
    headers.append("client_ID", client_ID);
    headers.append("subject", subject);
    headers.append("message", message);
    const opts : RequestOptions = new RequestOptions();
    opts.url = "http://localhost:8080/uk/ContactClient";
    opts.method  ="POST";
    opts.headers = headers;

    return this.http.request("POST", opts);
  }

  public deleteClient(staff_ID, clientID): Observable<any> {
    const headers : Headers = this.createHeaders(staff_ID);
    headers.append("client_ID", clientID)
    const opts : RequestOptions = new RequestOptions();
    opts.url = "http://localhost:8080/uk/DeleteClient";
    opts.method  ="DELETE";
    opts.headers = headers;

    return this.http.request("DELETE", opts);
  }
}
