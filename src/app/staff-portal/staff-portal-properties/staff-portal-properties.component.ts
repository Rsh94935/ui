import { Component, OnInit, Input } from '@angular/core';
import { StaffPortalPropertiesService } from './staff-portal-properties-service';
import { throwIfEmpty } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'app-staff-portal-properties',
  templateUrl: './staff-portal-properties.component.html',
  styleUrls: ['./staff-portal-properties.component.scss']
})
export class StaffPortalPropertiesComponent implements OnInit {
  @Input() staff;
  @Input() staffDetails;
  @Input() key;
  propertyList = [];
  modalHeader;
  modalProperty;
  modalSale;
  images;
  active = 0;
  deletedIDs;

  constructor(private spp: StaffPortalPropertiesService) { }

  ngOnInit() {
    this.modalHeader = '';
    this.images = this.attachmentsObject();
    this.setPropertyList();
  }

  private setPropertyList() {
    this.propertyList = [];
    this.spp.getPropertyList(this.staff['staff_ID']).subscribe(data => {
      this.propertyList = JSON.parse(data['_body']);
      this.propertyList.forEach(property => {
        if ( property['images'] !== "" ) {
          property['images'] = JSON.parse(property['images']);
        }
      });
    })
  }

  public createGrid() {
    let rows = "";

    this.propertyList.forEach(res => {
      if ( rows === "" ) {
        rows = "40px";
      } else {
        rows += " 40px";
      }
    })

    const style = {
      'display': 'grid',
      'grid-template-columns': '1fr',
      'grid-template-rows' : rows
    }

    return style;
  }

  public checkRights(rights) {
    if ( this.staff['internal'] === false ) {
      return false;
    } else if( this.staffDetails['admin']) {
      return true;
    } else if( this.staffDetails[rights] ) {
      return true;
    }

    return false;
  }

  public addImage() {
    document.getElementById("img").click();
  }

  public deleteImage(i: number) {
    this.deletedIDs.push(this.images['images'][i].id);
    this.images['images'].splice(i, 1);
  }

  public imageUploaded(event) {
    if (event.target.files && event.target.files[0]) {
      const file = {
        "name" : event.target.files[0].name,
        "fileSize" : event.target.files[0].size,
        "contents" : event.target.files[0],
        "id" : ""
      }
      this.images['files'].push(file);
    }
  }

  private attachmentsObject() {
    const att = {
      'folderId' : '',
      'files' : []
    }

    return att;
  }

  public createProperty() {
    this.active = 1;
    this.modalHeader = "Create new property";
    this.modalProperty = this.createEmptyProperty();
    this.modalSale = this.createEmptySale();
    this.images = this.attachmentsObject();
  }

  private createEmptyProperty() {
    const property = {
      'property_ID' : 0,
      'street_no' : '',
      'street_name' : '',
      'town' : '',
      'county' : '',
      'post_code' : '',
      'description' : '',
      'images' : '',
      'location' : '',
      'price' : '',
      'attachments' : ''
    }

    return property;
  }

  private createEmptySale() {
    const sale = {
      'sale_ID' : 0,
      'property_ID' : 0,
      'seller_ID' : 0,
      'buyer_ID' : 0,
      'staff_ID' : this.staff['staff_ID'],
      'sale_price' : 0.00
    }

    return sale;
  }

  public close() {
    this.modalProperty = this.createEmptyProperty();
    this.modalHeader = "";
  }

  public save() {
    if ( this.modalHeader === "Create new property" ) {
      this.create();
    } else if ( this.modalHeader === "Update property" ) {
      this.updateProperty();
    }
  }

  public create() {
    this.spp.createProperty(this.staff['staff_ID'], JSON.stringify(this.modalProperty)).subscribe(res => {
      const newProp = JSON.parse(res._body);
      this.propertyList.push(newProp);
      const updateProp = this.createEmptyProperty();
      updateProp['property_ID'] = newProp['property_ID'];
      this.modalSale['property_ID']=newProp['property_ID'];
      this.spp.createSale(this.staff['staff_ID'], JSON.stringify(this.modalSale)).subscribe(res => res);
      this.spp.createFolder(this.key, 'images-prop-' + updateProp['property_ID']).subscribe(res => {
        const retVal = JSON.parse(res._body);

        this.images['folderId'] = retVal['id'];
        updateProp['images'] = this.images;
        this.modalProperty = updateProp;
        this.updateProperty();
      });
    });
  }

  public test(files, start) {
    const file = files['files'][start];

    if ( file.contents !== '' ) {
      this.spp.uploadImageToGoogle(this.key, file, files['folderId']).subscribe(data => {
        const retVal = JSON.parse(data._body);
        file.id = retVal['id'];
        file.contents = '';
        files['files'][start] = file;
        if ( files['files'].length !== start + 1 ) {
          this.test(files, start + 1);
        } else {
          this.modalProperty['images'] = JSON.stringify(this.images);
          this.spp.updateProperty(this.staff['staff_ID'], JSON.stringify(this.modalProperty)).subscribe(res => {
            this.setPropertyList();
          });
        }
      });
    } else {
      if ( files['files'].length !== start + 1 ) {
        this.test(files, start + 1);
      } else {
        this.modalProperty['images'] = JSON.stringify(this.images);
        this.spp.updateProperty(this.staff['staff_ID'], JSON.stringify(this.modalProperty)).subscribe(res => {
          this.setPropertyList();
        });
      }
    }
  }

  public update(i: number) {
    this.modalHeader = "Update property";
    this.modalProperty = this.propertyList[i];
    this.spp.getSaleDetails(this.modalProperty['property_ID'], this.staff['staff_ID']).subscribe(res => {
      this.active = 1;
    });
    this.images = this.modalProperty['images'];
  }

  public updateProperty() {
    if ( this.images['files'].length > 0 || this.deletedIDs.length > 0 ) {
      this.test(this.images, 0);
      this.deletedIDs.forEach(id => {
        this.spp.deleteGDriveItem(this.key, id);
      })
    } else {
      this.spp.updateProperty(this.staff['staff_ID'], JSON.stringify(this.modalProperty)).subscribe(res => {
        this.setPropertyList();});
    }
  }

  public deleteProperty(i) {
    const folder = this.propertyList[i]['images']['folderId'];
    const id = this.propertyList[i]['property_ID'];
    this.spp.deleteSale(this.staff['staff_ID'], id).subscribe(res => {
      this.spp.deleteProperty(this.staff['staff_ID'], id).subscribe(res => {
        if ( res._body === "Property deleted" ) {
          if ( folder !== '' ) {
            this.spp.deleteGDriveItem(this.key, folder).subscribe(res => res);
          }
          const newList = [];
          let minus = 0;
          for ( let loopCounter = 0; loopCounter < this.propertyList.length; loopCounter++ ) {
            if ( loopCounter !== i ) {
              newList[loopCounter - minus] = this.propertyList[loopCounter];
            } else {
              minus = 1;
            }
          }
          this.propertyList = newList;
        }
      });
    });
  }
}
