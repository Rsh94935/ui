import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaffPortalPropertiesComponent } from './staff-portal-properties.component';

describe('StaffPortalPropertiesComponent', () => {
  let component: StaffPortalPropertiesComponent;
  let fixture: ComponentFixture<StaffPortalPropertiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaffPortalPropertiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaffPortalPropertiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
