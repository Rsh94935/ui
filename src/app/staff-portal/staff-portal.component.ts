import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { StaffPortalService } from './staff-portal.service';

@Component({
  selector: 'app-staff-portal',
  templateUrl: './staff-portal.component.html',
  styleUrls: ['./staff-portal.component.scss']
})
export class StaffPortalComponent implements OnInit {
  @Input() staff;
  @Output() loggingOut: EventEmitter<void> = new EventEmitter();
  key;
  staffDetails;
  page: string = 'home';
  initials: string = '';
  loaded = 0;

  constructor(private staffPortalService: StaffPortalService) { }

  ngOnInit() {
    this.initials = this.staff['first_name'].substring(0,1) + this.staff['last_name'].substring(0,1);
    this.staffPortalService.getGoogleAPIAuth().subscribe(res => {
      this.key = res._body;
    })
    this.staffPortalService.getStaffDetails(this.staff['internal'], this.staff['staff_ID']).subscribe(data => {
      this.staffDetails = JSON.parse(data._body);
      this.loaded = 1;
    });
  }

  public logout() {
    this.loggingOut.emit();
  }

}
