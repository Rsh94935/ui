import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaffPortalHomeComponent } from './staff-portal-home.component';

describe('StaffPortalHomeComponent', () => {
  let component: StaffPortalHomeComponent;
  let fixture: ComponentFixture<StaffPortalHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaffPortalHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaffPortalHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
