import { Component, OnInit, Input } from '@angular/core';
import { StaffPortalHomeService } from './staff-portal-home-service';

@Component({
  selector: 'app-staff-portal-home',
  templateUrl: './staff-portal-home.component.html',
  styleUrls: ['./staff-portal-home.component.scss']
})
export class StaffPortalHomeComponent implements OnInit {
  @Input() staff;
  sales = [];
  upcomingEvents = [];
  unassignedEvents = [];

  constructor(private sps: StaffPortalHomeService) { }

  ngOnInit() {
    this.sps.getMySales(this.staff['staff_ID']).subscribe(res => console.log(res));
    this.sps.getUnassignedEvents(this.staff['staff_ID']).subscribe(res => console.log(res));
    this.sps.getUpcomingEvents(this.staff['staff_ID']).subscribe(res => console.log(res));
  }

}
