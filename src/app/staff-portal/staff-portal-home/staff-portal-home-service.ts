import { Injectable } from '@angular/core';
import { RequestOptions, Http, Headers } from '@angular/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StaffPortalHomeService {

  constructor(private http: Http) { }

  public getUnassignedEvents(staff_ID): Observable<any> {
    const headers : Headers = this.createHeaders(staff_ID);
    const opts : RequestOptions = new RequestOptions();
    headers.append('start', "0");
    headers.append('amount', "5");
    opts.url = "http://localhost:8080/uk/GetUnassignedEvents";
    opts.method  ="GET";
    opts.headers = headers;

    return this.http.request("GET", opts);
  }

  public getUpcomingEvents(staff_ID): Observable<any> {
    const headers : Headers = this.createHeaders(staff_ID);
    const opts : RequestOptions = new RequestOptions();
    headers.append('start', "0");
    headers.append('amount', "5");
    opts.url = "http://localhost:8080/uk/GetUpcomingEvents";
    opts.method  ="GET";
    opts.headers = headers;

    return this.http.request("GET", opts);
  }

  public getMySales(staff_ID): Observable<any> {
    const headers : Headers = this.createHeaders(staff_ID);
    const opts : RequestOptions = new RequestOptions();
    headers.append('start', "0");
    headers.append('amount', "5");
    opts.url = "http://localhost:8080/uk/GetMySales";
    opts.method  ="GET";
    opts.headers = headers;

    return this.http.request("GET", opts);
  }

  private createHeaders(staff_ID) {
      const uuid = localStorage.getItem("staff_uuid");
      const headers : Headers = new Headers();
      headers.append('staff_ID', staff_ID);
      headers.append("Content-Type", "application/json")
      headers.append('Access-Control-Allow-Headers', 'Origin, Content-Type, X-Auth-Token, Access-Control-Allow-Headers, Access-Control-Allow-Origin, Access-Control-Allow-Methods');
      headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
      headers.append('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,DELETE,PUT');
      headers.append('uuid', uuid);

      return headers;
  }
}
