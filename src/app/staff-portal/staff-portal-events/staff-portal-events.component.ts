import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { StaffPortalEventsService } from './staff-portal-events-service';
import { throwIfEmpty } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'app-staff-portal-events',
  templateUrl: './staff-portal-events.component.html',
  styleUrls: ['./staff-portal-events.component.scss']
})
export class StaffPortalEventsComponent implements OnInit {
  @Input() staff;
  @Input() staffDetails;
  @Input() key;
  eventList = [];
  modalHeader;
  modalEvent;
  images;
  active = 0;
  deletedIDs;

  constructor(private spp: StaffPortalEventsService, private cdRef: ChangeDetectorRef) { }

  ngOnInit() {
    this.modalHeader = '';
    this.images = this.attachmentsObject();
    this.setEventList();
  }

  private setEventList() {
    this.eventList = [];
    this.spp.getEventList(this.staff['staff_ID']).subscribe(data => {
      this.eventList = JSON.parse(data['_body']);
      this.eventList.forEach(event => {
        const start = new Date(event['start_time']);
        const end = new Date(event['end_time']);
        event['start_time'] = this.setDate(start);
        event['end_time'] = this.setDate(end);
        if ( event['images'] !== "" ) {
          event['images'] = JSON.parse(event['images']);
        }
      });
    });
    this.active = 0;
  }

  private setDate(date : Date) {
    const year = date.getUTCFullYear();
    const month = (date.getUTCMonth().toString().length === 1 ? "0" : "") + (date.getUTCMonth() + 1); 
    const day = (date.getUTCDate().toString().length === 1 ? "0" : "") + (date.getUTCDate() + 1);
    const hour = (date.getUTCHours().toString().length === 1 ? "0" : "") + (date.getUTCHours()+1);
    const mins = (date.getUTCMinutes().toString().length === 1 ? "0" : "") + date.getUTCMinutes();

    return year + "-" + month + "-" + day + "T" + hour + ":" + mins;
  }

  public createGrid() {
    let rows = "";

    this.eventList.forEach(res => {
      if ( rows === "" ) {
        rows = "40px";
      } else {
        rows += " 40px";
      }
    })

    const style = {
      'display': 'grid',
      'grid-template-columns': '1fr',
      'grid-template-rows' : rows
    }

    return style;
  }

  public checkRights(rights) {
    if ( this.staff['internal'] === false ) {
      return true;
    }

    return false;
  }

  public canBeAssigned(i : number) {
    if ( this.eventList[i]['staff_manager_ID'] ) {
      return false;
    }
    if ( this.staff['internal'] === true ) {
      if ( this.staffDetails['amend_rights'] || this.staffDetails['admin'] ) {
        return true;
      }
    }

    return false;
  }

  public assignTo(i: number) {
    this.spp.assignTo(this.staff['staff_ID'], this.eventList[i]['event_ID']).subscribe(res => {
      const json = JSON.parse(res._body);
      const start = new Date(json['start_time']);
      const end = new Date(json['end_time']);
      json['start_time'] = this.setDate(start);
      json['end_time'] = this.setDate(end);
      if ( json['images'] !== "" ) {
        json['images'] = JSON.parse(json['images']);
      }
      this.eventList[i] = json;
    });
  }

  public addImage() {
    document.getElementById("img").click();
  }

  public deleteImage(i: number) {
    this.deletedIDs.push(this.images['images'][i].id);
    this.images['images'].splice(i, 1);
  }

  public imageUploaded(event) {
    if (event.target.files && event.target.files[0]) {
      const file = {
        "name" : event.target.files[0].name,
        "fileSize" : event.target.files[0].size,
        "contents" : event.target.files[0],
        "id" : ""
      }
      if ( this.images['files'] === undefined ) {
        this.images = this.attachmentsObject();
      }
      this.images['files'].push(file);
    }
  }

  private attachmentsObject() {
    const att = {
      'folderId' : '',
      'files' : []
    }

    return att;
  }

  public createEvent() {
    this.active = 1;
    this.modalHeader = "Create new event";
    this.modalEvent = this.createEmptyEvent();
    this.images = this.attachmentsObject();
    this.cdRef.detectChanges();
  }

  private createEmptyEvent() {
    const event = {
      'event_ID' : 0,
      'street_no' : '',
      'street_name' : '',
      'town' : '',
      'county' : '',
      'post_code' : '',
      'event_description' : '',
      'images' : '',
      'location' : '',
      'price' : '',
      'clients_attending' : '',
      'staff_manager_ID' : 0,
      'event_host_ID' : '',
      'start_time' : undefined,
      'end_time' : undefined
    }

    return event;
  }

  public close() {
    this.modalEvent = this.createEmptyEvent();
    this.modalHeader = "";
  }

  public checkFieldsPopulated() {
    let retVal = true;
    const requiredColumns = ['street_no', 'street_name', 'town', 'county', 'post_code', 'event_description', 'price', 'start_time', 'end_time'];

    for ( const key in this.modalEvent ) {
      if ( requiredColumns.includes(key) && !this.modalEvent[key] ) {
        return false;
      }
    }

    return retVal;
  }

  public save() {
    if ( this.modalHeader === "Create new event" ) {
      this.create();
    } else if ( this.modalHeader === "Update event" ) {
      this.updateEvent();
    }
  }

  public create() {
    this.modalEvent['start_time'] = new Date(String(this.modalEvent['start_time'].replace(/-/g,"/").replace('T', ' '))).getTime();
    this.modalEvent['end_time'] = new Date(String(this.modalEvent['end_time'].replace(/-/g,"/").replace('T', ' '))).getTime();
    this.modalEvent['event_host_ID'] = this.staff['staff_ID'];

    this.spp.createEvent(this.staff['staff_ID'], JSON.stringify(this.modalEvent)).subscribe(res => {
      const newProp = JSON.parse(res._body);
      this.eventList.push(newProp);
      const updateProp = this.createEmptyEvent();
      updateProp['event_ID'] = newProp['event_ID'];
      this.spp.createFolder(this.key, 'images-event-' + updateProp['event_ID']).subscribe(res => {
        const retVal = JSON.parse(res._body);

        this.images['folderId'] = retVal['id'];
        updateProp['images'] = this.images;
        this.modalEvent = updateProp;
        this.updateEvent();
      });
    });
  }

  public test(files, start) {
    const file = files['files'][start];

    if ( file.contents !== '' ) {
      this.spp.uploadImageToGoogle(this.key, file, files['folderId']).subscribe(data => {
        const retVal = JSON.parse(data._body);
        file.id = retVal['id'];
        file.contents = '';
        files['files'][start] = file;
        if ( files['files'].length !== start + 1 ) {
          this.test(files, start + 1);
        } else {
          this.modalEvent['images'] = JSON.stringify(this.images);
          this.spp.updateEvent(this.staff['staff_ID'], JSON.stringify(this.modalEvent)).subscribe(res => {
            this.setEventList();
          });
        }
      });
    } else {
      if ( files['files'].length !== start + 1 ) {
        this.test(files, start + 1);
      } else {
        this.modalEvent['images'] = JSON.stringify(this.images);
        this.spp.updateEvent(this.staff['staff_ID'], JSON.stringify(this.modalEvent)).subscribe(res => {
          this.setEventList();
        });
      }
    }
  }

  public update(i: number) {
    this.modalHeader = "Update event";
    this.modalEvent = this.eventList[i];
    this.active = 1;
    this.images = this.modalEvent['images'];
    this.cdRef.detectChanges();
  }

  public updateEvent() {
    if ( this.images['files'].length > 0 || this.deletedIDs.length > 0 ) {
      this.test(this.images, 0);
      this.deletedIDs.forEach(id => {
        this.spp.deleteGDriveItem(this.key, id);
      })
    } else {
      this.spp.updateEvent(this.staff['staff_ID'], JSON.stringify(this.modalEvent)).subscribe(res => {
        this.setEventList();});
    }
  }

  public deleteEvent(i) {
    const folder = this.eventList[i]['images']['folderId'];
    const id = this.eventList[i]['event_ID'];
    this.spp.deleteEvent(this.staff['staff_ID'], id, this.staff['internal']).subscribe(res => {
      if ( res._body === "Event deleted" ) {
        if ( folder !== '' ) {
          this.spp.deleteGDriveItem(this.key, folder).subscribe(res => res);
        }
        const newList = [];
        let minus = 0;
        for ( let loopCounter = 0; loopCounter < this.eventList.length; loopCounter++ ) {
          if ( loopCounter !== i ) {
            newList[loopCounter - minus] = this.eventList[loopCounter];
          } else {
            minus = 1;
          }
        }
        this.eventList = newList;
      }
    });
  }
}
