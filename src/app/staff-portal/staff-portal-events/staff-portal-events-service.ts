import { Injectable } from '@angular/core';
import { RequestOptions, Http, Headers } from '@angular/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StaffPortalEventsService {

  constructor(private http: Http) { }

  public getGoogleAPIAuth(staff_ID): Observable<any> {
    const headers : Headers = this.createHeaders(staff_ID);
    const opts : RequestOptions = new RequestOptions();
    opts.url = "http://localhost:8080/uk/GetAPIKey";
    opts.method  ="GET";
    opts.headers = headers;

    return this.http.request("GET", opts);
  }

  public getEventList(staff_ID): Observable<any> {
    const headers : Headers = this.createHeaders(staff_ID);
    headers.append('start', "0");
    headers.append('amount', "25");
    headers.append('currentDate', new Date().getTime().toString());
    const opts : RequestOptions = new RequestOptions();
    opts.url = "http://localhost:8080/uk/GetEventsRange";
    opts.method  ="GET";
    opts.headers = headers;

    return this.http.request("GET", opts);
  }

  private createHeaders(staff_ID) {
      const uuid = localStorage.getItem("staff_uuid");
      const headers : Headers = new Headers();
      headers.append('staff_ID', staff_ID);
      headers.append("Content-Type", "application/json")
      headers.append('Access-Control-Allow-Headers', 'Origin, Content-Type, X-Auth-Token, Access-Control-Allow-Headers, Access-Control-Allow-Origin, Access-Control-Allow-Methods');
      headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
      headers.append('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,DELETE,PUT');
      headers.append('uuid', uuid);

      return headers;
  }

  public createEvent(staff_ID, sEvent): Observable<any> {
    const headers : Headers = this.createHeaders(staff_ID);
    const opts : RequestOptions = new RequestOptions();
    opts.url = "http://localhost:8080/uk/AddEvent";
    opts.method  ="POST";
    opts.headers = headers;
    opts.body = sEvent;

    return this.http.request("POST", opts);
  }

  public updateEvent(staff_ID, sEvent): Observable<any> {
    const headers : Headers = this.createHeaders(staff_ID);
    const opts : RequestOptions = new RequestOptions();
    opts.url = "http://localhost:8080/uk/AmendEvent";
    opts.method  ="POST";
    opts.headers = headers;
    opts.body = sEvent;

    return this.http.request("POST", opts);
  }

  public deleteEvent(staff_ID, event_ID, internalType): Observable<any> {
    const headers : Headers = this.createHeaders(staff_ID);
    headers.append('event_ID', event_ID);
    headers.append('internalType', internalType);
    const opts : RequestOptions = new RequestOptions();
    opts.url = "http://localhost:8080/uk/DeleteEvent";
    opts.method  ="DELETE";
    opts.headers = headers;

    return this.http.request("DELETE", opts);
  }

  public uploadImageToGoogle(key, image, folder): Observable<any> {
    const metadata = {
      name : image.name,
      parents : [folder]
    }
    const headers : Headers = new Headers();
    const formData : FormData = new FormData();
    formData.append("", image.contents, image.name);
    formData.append("metadata", new Blob([JSON.stringify(metadata)], {type: 'application/json'}));
    headers.append('Authorization', key);
    const opts : RequestOptions = new RequestOptions();
    opts.url = "https://www.googleapis.com/upload/drive/v3/files";
    opts.method  ="POST";
    opts.headers = headers;
    opts.body = formData;

    return this.http.request("POST", opts);
  }

  public createFolder(key, folderName): Observable<any> {
    const metadata = {
      name : folderName,
      mimeType : 'application/vnd.google-apps.folder'
    }
    const headers : Headers = new Headers();
    const formData : FormData = new FormData();
    formData.append("resource", new Blob([JSON.stringify(metadata)], {type: 'application/json'}));
    headers.append('Authorization', key);
    const opts : RequestOptions = new RequestOptions();
    opts.url = "https://www.googleapis.com/upload/drive/v3/files";
    opts.method  ="POST";
    opts.headers = headers;
    opts.body = formData;

    return this.http.request("POST", opts);
  }

  public deleteGDriveItem(key, id): Observable<any> {
    const headers : Headers = new Headers();
    const formData : FormData = new FormData();
    headers.append('Authorization', key);
    const opts : RequestOptions = new RequestOptions();
    opts.url = "https://www.googleapis.com/drive/v3/files/" + id;
    opts.method  ="DELETE";
    opts.headers = headers;
    opts.body = formData;

    return this.http.request("DELETE", opts);
  }

  public assignTo(staff_ID, i): Observable<any> {
    const headers : Headers = this.createHeaders(staff_ID);
    headers.append("event_ID", i);
    const opts : RequestOptions = new RequestOptions();
    opts.url = "http://localhost:8080/uk/AddManager";
    opts.method  ="POST";
    opts.headers = headers;

    return this.http.request("POST", opts);
  }
}
