import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaffPortalEventsComponent } from './staff-portal-events.component';

describe('StaffPortalEventsComponent', () => {
  let component: StaffPortalEventsComponent;
  let fixture: ComponentFixture<StaffPortalEventsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaffPortalEventsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaffPortalEventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
