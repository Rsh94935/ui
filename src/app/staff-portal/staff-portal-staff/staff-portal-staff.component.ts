import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { StaffPortalStaffService } from './staff-portal-staff-service';

@Component({
  selector: 'app-staff-portal-staff',
  templateUrl: './staff-portal-staff.component.html',
  styleUrls: ['./staff-portal-staff.component.scss']
})
export class StaffPortalStaffComponent implements OnInit {
  @Input() staff;
  @Input() staffDetails;
  staffList = [];
  modalHeader;
  modalStaff;
  modalStaffDetails;
  active = 0;

  constructor(private sps: StaffPortalStaffService, private cdRef: ChangeDetectorRef) { }

  ngOnInit() {
    this.modalHeader = '';
    this.sps.getStaffList(this.staff['staff_ID']).subscribe(data => {
      this.staffList = JSON.parse(data['_body']);
    })
  }

  public createGrid() {
    let rows = "";

    this.staffList.forEach(res => {
      if ( rows === "" ) {
        rows = "40px";
      } else {
        rows += " 40px";
      }
    })

    const style = {
      'display': 'grid',
      'grid-template-columns': '1fr',
      'grid-template-rows' : rows
    }

    return style;
  }

  public checkRights() {
    if ( this.staff['internal'] === false ) {
      return true;
    } else if( this.staffDetails['admin'] ) {
      return true;
    }

    return false;
  }

  public createStaff() {
    this.active = 1;
    this.modalHeader = "Create new Staff member";
    this.modalStaff = this.createEmptyStaff();
    this.modalStaffDetails = this.createContact();
  }

  public viewStaff(i: number) {
    this.sps.getStaffDetails(this.staff['staff_ID'], this.staff['internal'], this.staffList[i]['staff_ID'], this.staffList[i]['internal']).subscribe(res => {
      this.modalStaffDetails = JSON.parse(res._body);
      console.log(this.modalStaffDetails)
      this.modalStaff = this.staffList[i];
      this.active = 1;
      this.modalHeader = "View staff details"
    })
  }

  private createEmptyStaff() {
    const staff = {
      'staff_ID' : 0,
      'first_name' : '',
      'last_name' : '',
      'email_address' : '',
      'pw' : '',
      'internal' : false,
      'sessionUUID' : '',
      'loggedIn' : 0
    }

    return staff;
  }

  public setInternal(bool: boolean) {
    this.modalStaff['internal'] = bool;
    this.modalStaffDetails = bool ? this.createInternal() : this.createContact();
  }

  private createContact() {
    const details = {
      "staff_ID" : this.modalStaff['staff_ID'],
      "company_name" : "",
      "hourly_rate" : ""
    }

    return details;
  }

  private createInternal() {
    const details = {
      "staff_ID" : this.modalStaff['staff_ID'],
      "read_rights" : true,
      "amend_rights" : false,
      "create_rights" : false,
      "delete_rights" : false,
      "admin" : false
    }

    return details;
  }

  public close() {
    this.modalStaff = this.createEmptyStaff();
    this.modalHeader = "";
  }

  public save() {
    if ( this.modalHeader === "Create new Staff member" ) {
      if ( this.staffDetails['create_rights'] ) {
        this.create();
      } else {
        alert("Insufficient rights: Not permitted to create staff members");
      }
    } else if ( this.modalHeader === "Update staff details" ) {
      if ( this.staffDetails['amend_rights'] ) {
        this.updateStaff();
      } else {
        alert("Insufficient rights: Not permitted to amend staff members");
      }
    }
  }

  public create() {
    this.sps.createStaff(this.staff['staff_ID'], JSON.stringify(this.modalStaff)).subscribe(res => {
      this.staffList.push(JSON.parse(res._body));
      this.modalStaff = JSON.parse(res._body);
      this.modalStaffDetails['staff_ID'] = this.modalStaff['staff_ID'];
      this.active = 0;
      this.sps.updateStaffDetails(this.staff['staff_ID'], JSON.stringify(this.modalStaffDetails), this.modalStaffDetails['internal']).subscribe(res => res);
    });
  }

  public update(i: number) {
    this.sps.getStaffDetails(this.staff['staff_ID'], this.staff['internal'], this.staffList[i]['staff_ID'], this.staffList[i]['internal']).subscribe(res => {
      this.modalStaffDetails = JSON.parse(res._body);
      this.modalStaff = this.staffList[i];
      this.active = 1;
      this.modalHeader = "Update staff details"
    })
  }

  public updateStaff() {
    this.sps.updateStaff(this.staff['staff_ID'], JSON.stringify(this.modalStaff)).subscribe(res => {
      this.sps.updateStaffDetails(this.staff['staff_ID'], JSON.stringify(this.modalStaffDetails), this.modalStaff['internal']).subscribe(res => {

      });
    });
  }

  public deleteStaff(i) {
    this.sps.deleteStaff(this.staff['staff_ID'], this.staff['internal'], this.staffList[i]['staff_ID'], this.staffList[i]['internal']).subscribe(res => {
      if ( res._body === "Staff member deleted" ) {
        const newList = [];
        let minus = 0;
        for ( let loopCounter = 0; loopCounter < this.staffList.length; loopCounter++ ) {
          if ( loopCounter !== i ) {
            newList[loopCounter - minus] = this.staffList[loopCounter];
          } else {
            minus = 1;
          }
        }
        this.staffList = newList;
      }
    });
  }
}
