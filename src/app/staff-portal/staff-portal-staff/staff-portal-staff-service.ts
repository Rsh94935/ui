import { Injectable } from '@angular/core';
import { RequestOptions, Http, Headers } from '@angular/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StaffPortalStaffService {

  constructor(private http: Http) { }

  public getStaffList(staff_ID): Observable<any> {
    const headers : Headers = this.createHeaders(staff_ID);
    const opts : RequestOptions = new RequestOptions();
    headers.append('start', "0");
    headers.append('amount', "25");
    opts.url = "http://localhost:8080/uk/GetStaffBasedOnInternal";
    opts.method  ="GET";
    opts.headers = headers;

    return this.http.request("GET", opts);
  }

  public getStaffDetails(staff_ID, internalType, search_ID, searchInternalType): Observable<any> {
    const headers : Headers = this.createHeaders(staff_ID);
    const opts : RequestOptions = new RequestOptions();
    headers.append('internalType' , internalType);
    headers.append('searchInternalType', searchInternalType);
    headers.append('search_ID', search_ID)
    opts.url = "http://localhost:8080/uk/GetStaffDetails";
    opts.method  ="GET";
    opts.headers = headers;

    return this.http.request("GET", opts);
  }

  private createHeaders(staff_ID) {
      const uuid = localStorage.getItem("staff_uuid");
      const headers : Headers = new Headers();
      headers.append('staff_ID', staff_ID);
      headers.append("Content-Type", "application/json")
      headers.append('Access-Control-Allow-Headers', 'Origin, Content-Type, X-Auth-Token, Access-Control-Allow-Headers, Access-Control-Allow-Origin, Access-Control-Allow-Methods');
      headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
      headers.append('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,DELETE,PUT');
      headers.append('uuid', uuid);

      return headers;
  }

  public createStaff(staff_ID, sStaff): Observable<any> {
    const headers : Headers = this.createHeaders(staff_ID);
    const opts : RequestOptions = new RequestOptions();
    opts.url = "http://localhost:8080/uk/AddStaff";
    opts.method  ="POST";
    opts.headers = headers;
    opts.body = sStaff;

    return this.http.request("POST", opts);
  }

  public updateStaff(staff_ID, sStaff): Observable<any> {
    const headers : Headers = this.createHeaders(staff_ID);
    const opts : RequestOptions = new RequestOptions();
    opts.url = "http://localhost:8080/uk/UpdateStaffDetails";
    opts.method  ="POST";
    opts.headers = headers;
    opts.body = sStaff;

    return this.http.request("POST", opts);
  }

  public updateStaffDetails(staff_ID, sStaffDetails, internalType): Observable<any> {
    const headers : Headers = this.createHeaders(staff_ID);
    const opts : RequestOptions = new RequestOptions();
    if ( internalType ) {
      opts.url = "http://localhost:8080/uk/UpdateInternalStaffDetails"
    } else {
      opts.url = "http://localhost:8080/uk/UpdateContactDetails"
    }
    opts.method  ="POST";
    opts.headers = headers;
    opts.body = sStaffDetails;

    return this.http.request("POST", opts);
  }

  public deleteStaff(staff_ID, internal, delete_ID, deleteType): Observable<any> {
    const headers : Headers = this.createHeaders(staff_ID);
    headers.append('internalType', internal);
    headers.append('delete_ID', delete_ID);
    headers.append('deleteType', deleteType);
    const opts : RequestOptions = new RequestOptions();
    opts.url = "http://localhost:8080/uk/DeleteStaff";
    opts.method  ="POST";
    opts.headers = headers;

    return this.http.request("POST", opts);
  }
}
