import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaffPortalStaffComponent } from './staff-portal-staff.component';

describe('StaffPortalStaffComponent', () => {
  let component: StaffPortalStaffComponent;
  let fixture: ComponentFixture<StaffPortalStaffComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaffPortalStaffComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaffPortalStaffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
