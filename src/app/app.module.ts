import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { ClientPortalComponent } from './clientPortal/client-portal.component';
import { PropertiesComponent } from './clientPortal/properties/properties.component';
import { AboutUsComponent } from './clientPortal/about-us/about-us.component';
import { EventsComponent } from './clientPortal/events/events.component';
import { ServicesComponent } from './clientPortal/services/services.component';
import { HomeComponent } from './clientPortal/home/home.component';
import { StaffPortalComponent } from './staff-portal/staff-portal.component';
import { StaffPortalHomeComponent } from './staff-portal/staff-portal-home/staff-portal-home.component';
import { StaffPortalStaffComponent } from './staff-portal/staff-portal-staff/staff-portal-staff.component';
import { StaffPortalPropertiesComponent } from './staff-portal/staff-portal-properties/staff-portal-properties.component';
import { StaffPortalEventsComponent } from './staff-portal/staff-portal-events/staff-portal-events.component';
import { StaffPortalServicesComponent } from './staff-portal/staff-portal-services/staff-portal-services.component';
import { StaffPortalClientsComponent } from './staff-portal/staff-portal-clients/staff-portal-clients.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ClientPortalComponent,
    PropertiesComponent,
    AboutUsComponent,
    EventsComponent,
    ServicesComponent,
    HomeComponent,
    StaffPortalComponent,
    StaffPortalHomeComponent,
    StaffPortalStaffComponent,
    StaffPortalPropertiesComponent,
    StaffPortalEventsComponent,
    StaffPortalServicesComponent,
    StaffPortalClientsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
