import { Injectable } from '@angular/core';
import { RequestOptions, Http, Headers } from '@angular/http'; 
import { map, publishReplay, refCount } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: Http) { }

  public checkSignedIn(uuid: string): Observable<any> {
    const headers : Headers = new Headers();
    const opts : RequestOptions = new RequestOptions();
    headers.append('uuid', uuid);
    headers.append("Content-Type", "application/json")
    headers.append('Access-Control-Allow-Headers', 'Origin, Content-Type, X-Auth-Token, Access-Control-Allow-Headers, Access-Control-Allow-Origin, Access-Control-Allow-Methods');
    headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
    headers.append('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,DELETE,PUT');
    opts.url = "http://localhost:8080/uk/ClientSessionStatus";
    opts.method  ="GET";
    opts.headers = headers;

    return this.http.request("GET", opts);
  }

  public checkStaffSignedIn(uuid: string): Observable<any> {
    const headers : Headers = new Headers();
    const opts : RequestOptions = new RequestOptions();
    headers.append('uuid', uuid);
    headers.append("Content-Type", "application/json")
    headers.append('Access-Control-Allow-Headers', 'Origin, Content-Type, X-Auth-Token, Access-Control-Allow-Headers, Access-Control-Allow-Origin, Access-Control-Allow-Methods');
    headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
    headers.append('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,DELETE,PUT');
    opts.url = "http://localhost:8080/uk/StaffSessionStatus";
    opts.method  ="GET";
    opts.headers = headers;

    return this.http.request("GET", opts);
  }

  public clientLogin(username: string, pw: string): Observable<any> {
    const headers : Headers = new Headers();
    const opts : RequestOptions = new RequestOptions();
    headers.append("Content-Type", "application/json")
    headers.append('username', username);
    headers.append('Access-Control-Allow-Headers', 'Origin, Content-Type, X-Auth-Token, Access-Control-Allow-Headers, Access-Control-Allow-Origin, Access-Control-Allow-Methods');
    headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
    headers.append('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,DELETE,PUT');
    headers.append('pw', pw);
    opts.url = "http://localhost:8080/uk/ClientLogin";
    opts.method  ="GET";
    opts.headers = headers;

    return this.http.request("GET", opts);
  }

  public staffLogin(username: string, pw: string): Observable<any> {
    const headers : Headers = new Headers();
    const opts : RequestOptions = new RequestOptions();
    headers.append("Content-Type", "application/json")
    headers.append('username', username);
    headers.append('Access-Control-Allow-Headers', 'Origin, Content-Type, X-Auth-Token, Access-Control-Allow-Headers, Access-Control-Allow-Origin, Access-Control-Allow-Methods');
    headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
    headers.append('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,DELETE,PUT');
    headers.append('pw', pw);
    opts.url = "http://localhost:8080/uk/StaffLogin";
    opts.method  ="GET";
    opts.headers = headers;

    return this.http.request("GET", opts);
  }

  public createNewUser(sClient: string): Observable<any> {
    const headers: Headers = new Headers();
    const opts : RequestOptions = new RequestOptions();
    headers.append("Content-Type", "application/json");
    headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');

    headers.append('Access-Control-Allow-Headers', 'Origin, Content-Type, X-Auth-Token, Access-Control-Allow-Headers, Access-Control-Allow-Origin, Access-Control-Allow-Methods');
    headers.append('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,DELETE,PUT');
    opts.url = "http://localhost:8080/uk/AddClient";
    opts.method  ="POST";
    opts.headers = headers;
    opts.body = sClient;

    return this.http.request("POST", opts);
  }
}
