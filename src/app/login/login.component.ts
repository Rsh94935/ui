import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @Output() loggedIn: EventEmitter<void> = new EventEmitter();
  @Output() staffPortal: EventEmitter<void> = new EventEmitter();
  sMessage: string = "";
  bCreateAccount: boolean = false;
  bStaff: boolean = false;
  sLoginUser: string = "";
  sLoginPw: string = "";
  fName: string = "";
  lName: string = "";
  streetNo: string = "";
  streetName: string = "";
  town: string = "";
  county: string = "";
  postCode: string = "";
  email: string = "";
  pw: string = "";


  constructor(private ls: LoginService) { }

  ngOnInit() {
  }

  public login() {
    this.sMessage = "";
    this.bStaff ? this.staffLogin() : this.clientLogin();
  }

  private clientLogin() {
    this.ls.clientLogin(this.sLoginUser, this.sLoginPw).subscribe(data => {
      if ( data.status === 200 ) {
        const json = JSON.parse(data._body);
        if ( json['client_ID'] === null || json['client_ID'] === 0 ) {
          this.sMessage = "Incorrect password, please try again";
        } else {
          localStorage.setItem("uuid", json['uuid']);
          this.loggedIn.emit(json);
        }
      }
    },
    error => {
      if( error.status === 400 ) {
        this.sMessage = "Username not known, please try again";
      } else {
        this.sMessage = "Error logging in"
      }
    });
  }

  private staffLogin() {
    this.ls.staffLogin(this.sLoginUser, this.sLoginPw).subscribe(data => {
      if ( data.status === 200 ) {
        const json = JSON.parse(data._body);
        if ( json['staff_ID'] === null || json['staff_ID'] === 0 ) {
          this.sMessage = "Incorrect password, please try again";
        } else {
          localStorage.setItem("staff_uuid", json['sessionUUID']);
          this.staffPortal.emit(json);
        }
      }
    },
    error => {
      if(error.status === 400) {
        this.sMessage = "Username not known, please try again"
      } else {
        this.sMessage = "Error logging in"
      }
    });
  }

  public createAccount() {
    const oClient = {
      client_ID: 0,
      first_name: this.fName,
      last_name: this.lName,
      street_no: this.streetNo,
      street_name: this.streetName,
      town: this.town,
      county: this.county,
      post_code: this.postCode,
      subscribed: true,
      email_address: this.email,
      pw: this.pw,
      uuid: "",
      logged_in: ""
    };
    
    this.ls.createNewUser(JSON.stringify(oClient)).subscribe(res => {
      if ( res.status === 200 ) {
        this.bCreateAccount = false;
        this.sMessage = "Account created!";
      }
    });
  }

}
