import { Component, OnInit } from '@angular/core';
import { ServicesService } from './services.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit {
  serviceList = [];
  searchlocation;
  hPrice = 1000;
  lPrice = 0;

  constructor(private ss: ServicesService, private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.ss.getEventList("", "", this.hPrice, this.lPrice).subscribe(res => {
      this.setServiceList(res);
    });
  }

  private setServiceList(list) {
    this.serviceList = JSON.parse(list._body);
    this.serviceList.forEach(service => {
      if ( service.images !== '' ) {
        service.images = JSON.parse(service.images);
      }
    });
  }

  public checkForImages(service) {
    if ( service['images'] = '' ) {
      return true;
    } else {
      return false;
    }
  }

  public getLink(service) {
    return "https://drive.google.com/uc?id=" + service['images']['files'][0].id;
  }

  public setMap(service) {
    return this.sanitizer.bypassSecurityTrustResourceUrl("https://www.google.com/maps/embed?pb=" + service['location']);
  }

  public signUpToEvent(i : number) {
    this.ss.addAttendee(this.serviceList[i]['service_ID']).subscribe(res => {
      const json = JSON.parse(res._body);
      if ( json.images !== '' ) {
        json.images = JSON.parse(json.images);
      }
       this.serviceList[i] = json;
    })
  }

  public applyFilters() {
    let area = "";
    if ( document.getElementById('town')['checked'] ) {
      area = "town";
    } else if ( document.getElementById('county')['checked']) {
      area = "county";
    }
    this.ss.getEventList(area, this.searchlocation, this.hPrice, this.lPrice).subscribe(res => {
      this.setServiceList(res);
    });
  }
}
