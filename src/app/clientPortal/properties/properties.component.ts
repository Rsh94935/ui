import { Component, OnInit } from '@angular/core';
import { PropertiesService } from './properties.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-properties',
  templateUrl: './properties.component.html',
  styleUrls: ['./properties.component.scss']
})
export class PropertiesComponent implements OnInit {
  houseList = [];
  searchlocation;
  hPrice = 1000000;
  lPrice = 100000;
  stc: boolean = false;

  constructor(private ps: PropertiesService, private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.ps.getAvailableProperties("", "", this.hPrice, this.lPrice).subscribe(res => {
      this.setHouseList(res);
    });
  }

  private setHouseList(list) {
    this.houseList = JSON.parse(list._body);
    this.houseList.forEach(house => {
      if ( house.images !== '' ) {
        house.images = JSON.parse(house.images);
        console.log(house.images)
      }
    });
  }

  public checkForImages(house) {
    if ( typeof house['images'] == 'object' ) {
      return true;
    } else {
      return false;
    }
  }

  public getLink(house) {
    return "https://drive.google.com/uc?id=" + house['images']['files'][0].id;
  }

  public setMap(house) {
    return this.sanitizer.bypassSecurityTrustResourceUrl("https://www.google.com/maps/embed?pb=" + house['location']);
  }

  public toggleSTC() {
    this.stc = !this.stc;
  }

  public applyFilters() {
    let area = "";
    if ( document.getElementById('town')['checked'] ) {
      area = "town";
    } else if ( document.getElementById('county')['checked']) {
      area = "county";
    }
    if ( this.stc ) {
      this.ps.getAvailableProperties(area, this.searchlocation, this.hPrice, this.lPrice).subscribe(res => {
        this.setHouseList(res);
      });
    } else {
      this.ps.getPropertiesList(area, this.searchlocation, this.hPrice, this.lPrice).subscribe(res => {
        this.setHouseList(res);
      });
    }
  }
}
