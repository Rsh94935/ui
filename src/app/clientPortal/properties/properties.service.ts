import { Injectable } from '@angular/core';
import { RequestOptions, Http, Headers } from '@angular/http'; 
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PropertiesService {

  constructor(private http: Http) { }

  public getPropertiesList(area, searchlocation, hPrice, lPrice): Observable<any> {
    const headers : Headers = new Headers();
    const opts : RequestOptions = new RequestOptions();
    headers.append("Content-Type", "application/json");
    headers.append('from', "0");
    headers.append('to', "10");
    headers.append('hPrice', hPrice);
    headers.append('lPrice', lPrice);
    headers.append('locationType', area);
    headers.append('searchLocation', searchlocation);
    headers.append('Access-Control-Allow-Headers', 'Origin, Content-Type, X-Auth-Token, Access-Control-Allow-Headers, Access-Control-Allow-Origin, Access-Control-Allow-Methods');
    headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
    headers.append('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,DELETE,PUT');
    opts.url = "http://localhost:8080/uk/GetPropertyRange";
    opts.method  ="GET";
    opts.headers = headers;

    return this.http.request("GET", opts);
  }

  public getAvailableProperties(area, searchlocation, hPrice, lPrice): Observable<any> {
    const headers : Headers = new Headers();
    const opts : RequestOptions = new RequestOptions();
    headers.append("Content-Type", "application/json");
    headers.append('from', "0");
    headers.append('to', "10");
    headers.append('hPrice', hPrice);
    headers.append('lPrice', lPrice);
    headers.append('locationType', area);
    headers.append('searchLocation', searchlocation);
    headers.append('Access-Control-Allow-Headers', 'Origin, Content-Type, X-Auth-Token, Access-Control-Allow-Headers, Access-Control-Allow-Origin, Access-Control-Allow-Methods');
    headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
    headers.append('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,DELETE,PUT');
    opts.url = "http://localhost:8080/uk/GetAvailablePropertyRange";
    opts.method  ="GET";
    opts.headers = headers;

    return this.http.request("GET", opts);
  }
}
