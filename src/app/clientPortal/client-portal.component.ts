import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-client-portal',
  templateUrl: './client-portal.component.html',
  styleUrls: ['./client-portal.component.scss']
})
export class ClientPortalComponent implements OnInit {
  @Input() client;
  @Output() loggingOut: EventEmitter<void> = new EventEmitter();
  activatedRoute;
  name;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    localStorage.setItem("client_ID", this.client['client_ID']);
    this.route.queryParams.subscribe(params => {
      this.name = params['name'];
    });
  }

  public logout() {
    this.loggingOut.emit();
  }
}
