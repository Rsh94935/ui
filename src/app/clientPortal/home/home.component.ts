import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  hover = 0;

  constructor() { }

  ngOnInit() {
    
  }

  public goToPage(linkNo: number) {
    switch(linkNo) {
      case 1:
        document.getElementById('homeLink').click();
        break;
      case 2:
        document.getElementById('eventLink').click();
        break;
      case 3:
        document.getElementById('serviceLink').click();
        break;
    }
  }
}
