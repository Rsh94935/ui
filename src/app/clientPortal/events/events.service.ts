import { Injectable } from '@angular/core';
import { RequestOptions, Http, Headers } from '@angular/http'; 
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EventsService {

  constructor(private http: Http) { }

  public getEventList(area, searchlocation, hPrice, lPrice): Observable<any> {
    const headers : Headers = new Headers();
    const opts : RequestOptions = new RequestOptions();
    headers.append("Content-Type", "application/json");
    headers.append('start', "0");
    headers.append('amount', "10");
    headers.append('hPrice', hPrice);
    headers.append('lPrice', lPrice);
    headers.append('locationType', area);
    headers.append('searchLocation', searchlocation);
    headers.append('client_ID', localStorage.getItem('client_ID'));
    headers.append('currentDate', new Date().getMilliseconds().toString());
    headers.append('Access-Control-Allow-Headers', 'Origin, Content-Type, X-Auth-Token, Access-Control-Allow-Headers, Access-Control-Allow-Origin, Access-Control-Allow-Methods');
    headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
    headers.append('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,DELETE,PUT');
    opts.url = "http://localhost:8080/uk/GetAuthEventsRange";
    opts.method  ="GET";
    opts.headers = headers;

    return this.http.request("GET", opts);
  }

  public addAttendee(event_ID): Observable<any> {
    const headers : Headers = new Headers();
    const opts : RequestOptions = new RequestOptions();
    headers.append("Content-Type", "application/json");
    headers.append("event_ID", event_ID)
    headers.append('client_ID', localStorage.getItem('client_ID'));
    headers.append('uuid', localStorage.getItem("uuid"));
    headers.append('currentDate', new Date().getMilliseconds().toString());
    headers.append('Access-Control-Allow-Headers', 'Origin, Content-Type, X-Auth-Token, Access-Control-Allow-Headers, Access-Control-Allow-Origin, Access-Control-Allow-Methods');
    headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
    headers.append('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,DELETE,PUT');
    opts.url = "http://localhost:8080/uk/AddAttendee";
    opts.method  ="POST";
    opts.headers = headers;

    return this.http.request("POST", opts);
  }
}
