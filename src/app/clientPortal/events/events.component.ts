import { Component, OnInit } from '@angular/core';
import { EventsService } from './events.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss']
})
export class EventsComponent implements OnInit {
  eventList = [];
  searchlocation;
  hPrice = 1000;
  lPrice = 0;

  constructor(private es: EventsService, private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.es.getEventList("", "", this.hPrice, this.lPrice).subscribe(res => {
      this.setEventList(res);
    });
  }

  private setEventList(list) {
    this.eventList = JSON.parse(list._body);
    this.eventList.forEach(house => {
      if ( house.images !== '' ) {
        house.images = JSON.parse(house.images);
      }
    });
  }

  public checkForImages(event) {
    if ( typeof event['images'] == 'object' ) {
      return true;
    } else {
      return false;
    }
  }

  public getLink(house) {
    return "https://drive.google.com/uc?id=" + house['images']['files'][0].id;
  }

  public setMap(house) {
    return this.sanitizer.bypassSecurityTrustResourceUrl("https://www.google.com/maps/embed?pb=" + house['location']);
  }

  public signUpToEvent(i : number) {
    this.es.addAttendee(this.eventList[i]['event_ID']).subscribe(res => {
      const json = JSON.parse(res._body);
      if ( json.images !== '' ) {
        json.images = JSON.parse(json.images);
      }
       this.eventList[i] = json;
    })
  }

  public applyFilters() {
    let area = "";
    if ( document.getElementById('town')['checked'] ) {
      area = "town";
    } else if ( document.getElementById('county')['checked']) {
      area = "county";
    }
    this.es.getEventList(area, this.searchlocation, this.hPrice, this.lPrice).subscribe(res => {
      this.setEventList(res);
    });
  }
}
