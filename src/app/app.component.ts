import { Component,  OnInit } from '@angular/core';
import { LoginService } from './login/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  clientPortal: boolean = false;
  staffPortal: boolean = false;
  staff;
  client;
  title = 'website';

  constructor(private loginService: LoginService) { }

  ngOnInit() {
    this.checkSession();
  }

  private checkSession() {
    const staffUUID : string = localStorage.getItem("staff_uuid");
    if ( staffUUID === undefined || staffUUID === '' || staffUUID === null ) {
      const uuid : string = localStorage.getItem("uuid");
      if ( uuid === undefined || uuid === '' ) {
        this.clientPortal = false;
      } else {
        this.loginService.checkSignedIn(uuid).subscribe(res => {
          if ( res.status === 200 ) {
            const json = JSON.parse(res._body);
            if ( json['client_ID'] > 0 ) {
              this.client = json;
              this.clientPortal = true;
            } else {
              this.clientPortal = false;
              localStorage.removeItem("uuid");
            }
          }
        });
      }
    } else {
      this.loginService.checkStaffSignedIn(staffUUID).subscribe(res => {
        if ( res.status === 200 ) {
          const json = JSON.parse(res._body);
          if ( json['staff_ID'] > 0 ) {
            this.staff = json;
            this.staffPortal = true;
          } else {
            this.staffPortal = false;
            localStorage.removeItem("staff_uuid");
          }
        }
      })
    }
  }

  public loginToClientPortal(client) {
    this.clientPortal = true;
    this.client = client;
  }

  public loginToStaffPortal(staff) {
    this.staffPortal = true;
    this.staff = staff;
  }

  public logout() {
    this.clientPortal = false;
    this.staffPortal = false;
    this.staff = undefined;
    this.client = undefined;
    localStorage.removeItem("uuid");
    localStorage.removeItem("staff_uuid");
  }
}
